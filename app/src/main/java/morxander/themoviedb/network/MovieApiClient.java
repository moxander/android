package morxander.themoviedb.network;

import morxander.themoviedb.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by morxander on 1/20/17.
 */

public class MovieApiClient {

    private OkHttpClient.Builder okBuilder;
    private Retrofit.Builder adapterBuilder;

    public MovieApiClient() {
        createDefaultAdapter();
    }


    public void createDefaultAdapter() {
        okBuilder = new OkHttpClient.Builder();
        String baseUrl = BuildConfig.MOVIE_API_URL;
        adapterBuilder = new Retrofit
                .Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(JacksonConverterFactory.create());
    }

    public <S> S createService(Class<S> serviceClass) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        // for logging
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        return adapterBuilder
                .client(httpClient.build())
                .build()
                .create(serviceClass);

    }
}