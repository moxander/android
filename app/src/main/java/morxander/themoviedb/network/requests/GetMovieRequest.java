package morxander.themoviedb.network.requests;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import morxander.themoviedb.BuildConfig;
import morxander.themoviedb.R;
import morxander.themoviedb.TheMovieApp;
import morxander.themoviedb.network.MovieApi;
import morxander.themoviedb.network.MovieApiClient;
import morxander.themoviedb.network.models.NowPlayingMovies;
import morxander.themoviedb.utils.RequestListener;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by morxander on 1/20/17.
 */

public class GetMovieRequest extends AsyncTask {

    private RequestListener callBack;

    public GetMovieRequest(RequestListener callBack) {
        this.callBack = callBack;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        MovieApi api = new MovieApiClient().createService(MovieApi.class);
        Context context = TheMovieApp.getAppContext();
        Call<NowPlayingMovies> call = api.getMovies(BuildConfig.MOVIE_API_KEY, "en-US", 1);
        Response<NowPlayingMovies> response = null;
        try {
            response = call.execute();
            if (response.isSuccessful()) {
                NowPlayingMovies nowPlayingMovies = response.body();
            } else {
                Log.v("Network Failed", "Network Error : " + response.errorBody());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        Response<NowPlayingMovies> response;
        try {
            response = (Response<NowPlayingMovies>) o;
        } catch (Exception e) {
            response = null;
        }
        callBack.onFinished(response);
    }
}
