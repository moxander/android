package morxander.themoviedb.network;

import morxander.themoviedb.network.models.NowPlayingMovies;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by morxander on 1/20/17.
 */

public interface MovieApi {

    @GET("/3/movie/now_playing")
    Call<NowPlayingMovies> getMovies(
            @Query("api_key") String key,
            @Query("language") String language,
            @Query("page") int page
    );

}
