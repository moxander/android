package morxander.themoviedb.ui.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import morxander.themoviedb.R;
import morxander.themoviedb.ui.adapters.MoviesListAdapter;
import morxander.themoviedb.ui.models.MovieViewModel;
import morxander.themoviedb.ui.presenters.MainActivityPresenter;
import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusAppCompatActivity;

@RequiresPresenter(MainActivityPresenter.class)
public class MainActivity extends NucleusAppCompatActivity<MainActivityPresenter> {

    private ProgressDialog dialog;

    @BindView(R.id.mainactivity_list_movies)
    RecyclerView moviesRecyclerView;

    @BindView(R.id.mainactivity_empty_view)
    public LinearLayout emptyView;

    private MoviesListAdapter moviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.loading));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void updateView(List<MovieViewModel> movieViewModelList){
        moviesRecyclerView.setHasFixedSize(false);
        moviesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        moviesAdapter = new MoviesListAdapter(movieViewModelList, this);
        moviesRecyclerView.setAdapter(moviesAdapter);
    }
}
