package morxander.themoviedb.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import morxander.themoviedb.R;
import morxander.themoviedb.ui.presenters.MovieDetialsActivityPresenter;
import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusAppCompatActivity;

@RequiresPresenter(MovieDetialsActivityPresenter.class)
public class MovieDetialsActivity extends NucleusAppCompatActivity<MovieDetialsActivityPresenter> {

    @BindView(R.id.movie_detials_poster)
    ImageView moviePoster;

    @BindView(R.id.movie_detials_overview)
    TextView movieOverView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detials);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            setTitle(extras.getString("title"));
            getPresenter().updateMovie(extras.getString("title"),extras.getString("overview"),extras.getString("poster"));
        }
    }

    public void updateMovie(String title, String overView, String poster){
        setTitle(title);
        movieOverView.setText(overView);
        Picasso.with(this).load(poster).into(moviePoster);
    }

}
