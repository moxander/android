package morxander.themoviedb.ui.models;

import morxander.themoviedb.BuildConfig;
import morxander.themoviedb.network.models.MovieNetworkModel;
import morxander.themoviedb.network.models.NowPlayingMovies;

/**
 * Created by morxander on 1/20/17.
 */

public class MovieViewModel {

    String title;
    String overview;
    String poster;
    String releaseDate;
    String language;
    double vote;

    public MovieViewModel() {
    }

    public MovieViewModel(String title, String overview, String poster, String releaseDate, String language, float vote) {
        this.title = title;
        this.overview = overview;
        this.poster = poster;
        this.releaseDate = releaseDate;
        this.language = language;
        this.vote = vote;
    }

    public MovieViewModel(MovieNetworkModel movieViewModel) {
        this.title = movieViewModel.getTitle();
        this.overview = movieViewModel.getOverview();
        this.poster = movieViewModel.getPosterPath();
        this.releaseDate = movieViewModel.getReleaseDate();
        this.language = movieViewModel.getOriginalLanguage();
        this.vote = movieViewModel.getVoteAverage();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster() {
        return BuildConfig.MOVIE_API_IMAGE_PREFIX + poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public double getVote() {
        return vote;
    }

    public void setVote(double vote) {
        this.vote = vote;
    }
}
