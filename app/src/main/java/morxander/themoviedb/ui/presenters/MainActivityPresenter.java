package morxander.themoviedb.ui.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import morxander.themoviedb.network.models.MovieNetworkModel;
import morxander.themoviedb.network.models.NowPlayingMovies;
import morxander.themoviedb.network.requests.GetMovieRequest;
import morxander.themoviedb.ui.activities.MainActivity;
import morxander.themoviedb.ui.models.MovieViewModel;
import morxander.themoviedb.utils.RequestListener;
import nucleus.presenter.Presenter;
import retrofit2.Response;

/**
 * Created by morxander on 1/20/17.
 */

public class MainActivityPresenter extends Presenter<MainActivity> implements RequestListener {

    // [ 0 => ini, 1 => requesting network, 2 => done, 3 => network error ]
    private int status = 0;
    List<MovieViewModel> movieViewModelList;
    private GetMovieRequest getMovieRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
        movieViewModelList = new ArrayList<MovieViewModel>();
        getMovieRequest = new GetMovieRequest(this);
        getMovieRequest.execute();
        status = 1;
    }

    @Override
    protected void onTakeView(MainActivity mainActivity) {
        super.onTakeView(mainActivity);
        updateView();
    }

    private void updateView() {
        if(status == 0 || status == 1){
            getView().showDialog();
        }else if(status == 2){
            getView().hideDialog();
            getView().updateView(movieViewModelList);
        }
    }

    @Override
    public void onFinished(Object object) {
        Response<NowPlayingMovies> response;
        try {
            status = 2;
            response = (Response<NowPlayingMovies>) object;
            NowPlayingMovies nowPlayingMovies = response.body();
            List<MovieNetworkModel> movieNetworkModels = nowPlayingMovies.getMovieNetworkModels();
            for(int i=0;i<movieNetworkModels.size();i++){
                MovieViewModel movieViewModel = new MovieViewModel(movieNetworkModels.get(i));
                movieViewModelList.add(movieViewModel);
            }
            getView().updateView(movieViewModelList);
        } catch (Exception e) {
            response = null;
            status = 3;
        }
        updateView();
    }
}
