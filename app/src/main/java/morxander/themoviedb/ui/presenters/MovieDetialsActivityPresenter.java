package morxander.themoviedb.ui.presenters;

import android.os.Bundle;
import android.support.annotation.Nullable;

import morxander.themoviedb.ui.activities.MovieDetialsActivity;
import nucleus.presenter.Presenter;

/**
 * Created by morxander on 1/20/17.
 */

public class MovieDetialsActivityPresenter extends Presenter<MovieDetialsActivity> {

    String title;
    String overView;
    String poster;

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
    }

    @Override
    protected void onTakeView(MovieDetialsActivity movieDetialsActivity) {
        super.onTakeView(movieDetialsActivity);
        updateMovie();
    }

    public void updateMovie(String title, String overView, String poster){
        this.title = title;
        this.poster = poster;
        this.overView = overView;
    }

    public void updateMovie(){
        if(getView() != null){
            getView().updateMovie(title,overView,poster);
        }
    }

    public String getTitle() {
        return title;
    }

    public String getOverView() {
        return overView;
    }

    public String getPoster() {
        return poster;
    }
}
