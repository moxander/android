package morxander.themoviedb.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import morxander.themoviedb.R;
import morxander.themoviedb.ui.adapters.view_holders.MovieViewHolder;
import morxander.themoviedb.ui.models.MovieViewModel;

/**
 * Created by morxander on 1/20/17.
 */

public class MoviesListAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private final List<MovieViewModel> moviesList;
    private final LayoutInflater inflater;
    private final Context context;

    public MoviesListAdapter(List<MovieViewModel> offerItemList, Context context) {
        this.moviesList = offerItemList;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.movie_row, parent, false);
        return new MovieViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bindView(inflater.getContext(), moviesList.get(position));
    }

    @Override
    public int getItemCount() {
        return moviesList == null ? 0 : moviesList.size();
    }
}
