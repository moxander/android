package morxander.themoviedb.ui.adapters.view_holders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import morxander.themoviedb.R;
import morxander.themoviedb.TheMovieApp;
import morxander.themoviedb.ui.activities.MainActivity;
import morxander.themoviedb.ui.activities.MovieDetialsActivity;
import morxander.themoviedb.ui.models.MovieViewModel;

/**
 * Created by morxander on 1/20/17.
 */

public class MovieViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.movie_row_poster)
    ImageView moviePoster;

    @BindView(R.id.movie_row_title)
    TextView movieTitle;

    @BindView(R.id.movie_row_overview)
    TextView movieOverView;

    @BindView(R.id.movie_row_container)
    LinearLayout movieContainer;

    Context context;

    public MovieViewHolder(final View itemView, Context context) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;
    }

    public void bindView(final Context context, final MovieViewModel movieViewModel) {
        movieTitle.setText(movieViewModel.getTitle());
        movieOverView.setText(movieViewModel.getOverview().substring(0,20) + "....");
        Picasso.with(context).load(movieViewModel.getPoster()).resize(150, 220).centerCrop().into(moviePoster);
        movieContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MovieDetialsActivity.class);
                intent.putExtra("title", movieViewModel.getTitle());
                intent.putExtra("poster", movieViewModel.getPoster());
                intent.putExtra("overview", movieViewModel.getOverview());
                context.startActivity(intent);
            }
        });

    }
}
