package morxander.themoviedb;

import android.app.Application;
import android.content.Context;

/**
 * Created by morxander on 1/20/17.
 */

public class TheMovieApp extends Application {

    private static Context context;

    public static Context getAppContext() {
        return TheMovieApp.context;
    }

    public void onCreate() {
        super.onCreate();
        TheMovieApp.context = getApplicationContext();
    }
}
