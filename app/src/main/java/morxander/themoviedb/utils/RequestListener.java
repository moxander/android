package morxander.themoviedb.utils;

/**
 * Created by morxander on 1/20/17.
 */

public interface RequestListener {
    public void onFinished(Object object);
}
